Remote CUDA Support
===================

This project provides techniques for interposing and marshaling the NVIDIA CUDA
Runtime API.
