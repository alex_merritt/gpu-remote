#ifndef CUDA_ASTCONSUMER_INCLUDED
#define CUDA_ASTCONSUMER_INCLUDED

/* Clang includes */
#include "clang/AST/RecursiveASTVisitor.h"
#include "clang/AST/ASTConsumer.h"

/* Local includes */
#include "cudaRecursiveASTVisitor.hpp"

class cudaASTConsumer :
    public clang::ASTConsumer
{
    public:
        cudaASTConsumer(const char *f);
        ~cudaASTConsumer();

        virtual bool HandleTopLevelDecl(clang::DeclGroupRef d);

        cudaRecursiveASTVisitor *visitor;

    private:
        /* prevent initialization w/o filename */
        cudaASTConsumer(void);
};

#endif /* CUDA_ASTCONSUMER_INCLUDED */
