#include <cstddef>  /* size_t */
#include <math.h>   /* pull in defs needed by below includes */
#include <limits.h> /* more defs */

/* expose internal NVCC-generated functions */
#define __CUDA_INTERNAL_COMPILATION__ donkey_punch

#include <cuda_runtime_api.h>   /* the public api */
#include <__cudaFatFormat.h>    /* structure defs for internal funcs */
#include <crt/host_runtime.h>   /* file containing internal funcs */

int main(void) { return 0; }
