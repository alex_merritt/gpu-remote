#ifndef CUDA_RECURSIVEASTVISITOR_INCLUDED
#define CUDA_RECURSIVEASTVISITOR_INCLUDED

#include <sstream>

#include "clang/AST/RecursiveASTVisitor.h"
#include "clang/Frontend/CompilerInstance.h"

class cudaRecursiveASTVisitor :
    public clang::RecursiveASTVisitor<cudaRecursiveASTVisitor>
{
    public:
        cudaRecursiveASTVisitor();
        ~cudaRecursiveASTVisitor();

        clang::CompilerInstance *compiler;
        bool VisitFunctionDecl(clang::FunctionDecl *f);
        bool VisitRecordDecl(clang::RecordDecl *recDecl);
        bool VisitTypedefNameDecl(clang::TypedefNameDecl *tdefDecl);
        bool VisitDecl(clang::Decl *d); /* only used for investigating */
        /* put other methods here to visit other things in the AST */

    private:
        // Only RecordDecl and TypedefNameDecl subclasses will be present in
        // this structure (see implementations of VisitRecordDecl and
        // VisitTypedefNameDecl).
        typedef std::map<std::string, clang::TypeDecl*> typeMap_t;
        typedef std::pair<std::string, clang::TypeDecl*> typePair_t;
        typeMap_t records;

        typedef std::map<std::string, clang::FunctionDecl*> funcMap_t;
        typedef std::pair<std::string, clang::FunctionDecl*> funcPair_t;
        funcMap_t funcs;

        typedef llvm::raw_fd_ostream file_t;
        file_t      *cudaCFile, *cudaHFile, *structFile;
        std::string  cFileName,  hFileName,  sFileName;

        std::string jmpTblName;

        // TypedefNameDecl which resolve to a RecordDecl underneath might not
        // have a name for the record (i.e. the struct is defined with the
        // typedef directly) in which case name is passed as the name of the
        // typdef itself).
        void emitRecord(const clang::RecordDecl *recDecl,
                std::string name = std::string());
        void emitType(const clang::TypeDecl *typeDecl);

        int openCFile();
        int openHFile();
        int openSFile();
        void closeCFile();
        void closeHFile();
        void closeSFile();
        int openFile(const std::string name, llvm::raw_fd_ostream **file);
};

#endif /* CUDA_RECURSIVEASTVISITOR_INCLUDED */
