/* System includes */
#include <sstream>

/* LLVM includes */
#include "llvm/Config/config.h" /* CLANG_VERSION_STRING */
#include "llvm/Support/Host.h"

/* Clang includes */
#include "clang/Basic/TargetInfo.h"
#include "clang/AST/ASTConsumer.h"
#include "clang/Basic/Version.h" /* LLVM_PREFIX */
#include "clang/Parse/ParseAST.h"

/* Local includes */
#include "cudaASTConsumer.hpp"

using namespace clang;

//===----------------------------------------------------------------------===//
// Constructors (public)
//===----------------------------------------------------------------------===//

cudaASTConsumer::cudaASTConsumer(const char *f)
{
    visitor = new cudaRecursiveASTVisitor();
    CompilerInstance *c = visitor->compiler = new CompilerInstance();
    c->createDiagnostics();

    llvm::IntrusiveRefCntPtr<TargetOptions> pto(new TargetOptions());
    pto->Triple = llvm::sys::getDefaultTargetTriple();
    llvm::IntrusiveRefCntPtr<TargetInfo>
        pti(TargetInfo::CreateTargetInfo(c->getDiagnostics(), &(*pto)));
    c->setTarget(pti.getPtr());

    c->createFileManager();
    c->createSourceManager(c->getFileManager());

    HeaderSearchOptions &headerSearchOptions = c->getHeaderSearchOpts();
    //headerSearchOptions.ResourceDir = LLVM_PREFIX "/lib/clang/" CLANG_VERSION_STRING;
    //headerSearchOptions.ResourceDir = "/usr/local/lib/clang/3.2/include";

    // <Warning!!> -- Platform Specific Code lives here
    headerSearchOptions.AddPath("/usr/lib/clang/3.5/include",
            clang::frontend::Angled, false, false); /* size_t etc */
    headerSearchOptions.AddPath("/usr/include",
            clang::frontend::Angled, false, false);
    headerSearchOptions.AddPath("/usr/local/include",
            clang::frontend::Angled, false, false);
    headerSearchOptions.AddPath("/usr/include/linux",
            clang::frontend::Angled, false, false);
    headerSearchOptions.AddPath("/usr/local/cuda/include",
            clang::frontend::Angled, false, false);
    headerSearchOptions.AddPath("/usr/include/c++/4.8",
            clang::frontend::Angled, false, false);
    headerSearchOptions.AddPath("/usr/include/x86_64-linux-gnu/c++/4.8",
            clang::frontend::Angled, false, false);
    headerSearchOptions.AddPath("/usr/include/x86_64-linux-gnu",
            clang::frontend::Angled, false, false);
    //headerSearchOptions.AddPath("/usr/include/c++/4.4.4/tr1",
            //clang::frontend::Angled, false, false, false);
    // </Warning!!> -- End of Platform Specific Code

    c->getLangOpts().GNUMode = 1; 
    c->getLangOpts().CXXExceptions = 1; 
    c->getLangOpts().RTTI = 1; 
    c->getLangOpts().Bool = 1; 
    c->getLangOpts().CPlusPlus = 1;

    c->createPreprocessor();
    c->getPreprocessorOpts().UsePredefines = true;

    c->setASTConsumer(this);

    c->createASTContext();

    const FileEntry *pFile = c->getFileManager().getFile(f);
    c->getSourceManager().createMainFileID(pFile);
    c->getDiagnosticClient().BeginSourceFile(c->getLangOpts(),
            &c->getPreprocessor());

    // From the doxygen documentation:
    // "Parse the entire file specified, notifying the ASTConsumer as the file
    // is parsed. This operation inserts the parsed decls into the translation
    // unit held by Ctx."
    ParseAST(c->getPreprocessor(), this, c->getASTContext());

    c->getDiagnosticClient().EndSourceFile();

    delete c;
}

cudaASTConsumer::~cudaASTConsumer()
{
    delete visitor;
}

//===----------------------------------------------------------------------===//
// Member functions (public)
//===----------------------------------------------------------------------===//

/* called whenever a declaration is found */
bool cudaASTConsumer::HandleTopLevelDecl(DeclGroupRef d)
{
    typedef DeclGroupRef::iterator iter;
    for (iter b = d.begin(), e = d.end(); b != e; ++b)
        visitor->TraverseDecl(*b);
    return true; // keep going
}
