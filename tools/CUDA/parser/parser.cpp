/* based on CIrewriter.cpp (see readme) */

/* System includes */
#include <sys/stat.h>
#include <stdio.h>

/* Local includes */
#include "cudaASTConsumer.hpp"

int main(int argc, char *argv[])
{
    struct stat sb;
    const char *filename;

    if (argc != 2)
    {
        llvm::errs() << "Usage: " << *argv << " cpp_file [2>/dev/null]\n";
        llvm::errs() << "The file 'cudart.c' will be created.\n";
        return -1;
    }

    filename = argv[1];
    if (stat(filename, &sb) == -1)
    {
        perror(filename);
        exit(EXIT_FAILURE);
    }

    cudaASTConsumer *astConsumer = new cudaASTConsumer(filename);

    return 0;
}

