#! /usr/bin/env bash
# http://clang.llvm.org/get_started.html
set -e

if [ ! -d llvm ]; then
    svn co http://llvm.org/svn/llvm-project/llvm/tags/RELEASE_34/final llvm
fi

if [ ! -d llvm/tools/clang ]; then
    pushd llvm/tools
        svn co http://llvm.org/svn/llvm-project/cfe/tags/RELEASE_34/final clang
    popd
fi

if [ ! -d llvm/tools/lldb ]; then
    pushd llvm/tools
        svn co http://llvm.org/svn/llvm-project/lldb/tags/RELEASE_34/final lldb
    popd
fi


#if [ ! -d llvm/tools/clang/tools/extra ]; then
#    pushd llvm/tools/clang/tools
#        svn co http://llvm.org/svn/llvm-project/clang-tools-extra/trunk extra
#    popd
#fi

if [ ! -d llvm/projects/compiler-rt ]; then
    pushd llvm/projects
        svn co http://llvm.org/svn/llvm-project/compiler-rt/tags/RELEASE_34/final compiler-rt
    popd
fi

if [ -d build ]; then
    make -C build/ -j clean || true
else
    mkdir -v build
fi

num_cores=$(grep -c "physical id" /proc/cpuinfo)
echo Building ...
pushd build
    ../llvm/configure --enable-optimized --enable-cxx11
    make -j $(($num_cores + 1))
popd

# NOTES - LLDB Compilation
# If you see some linker error with libpython.2.7.a and a message to recompile
# with -fPIC, it means you didn't install python-devel or from source you didn't
# build python with shared libraries (./configure --enabled-shared).

