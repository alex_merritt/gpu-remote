#include <cuda_runtime_api.h>
#include <stdio.h>
int main(void)
{
    int numDevices;
    if (cudaGetDeviceCount(&numDevices) != cudaSuccess) {
        fprintf(stderr, "Error calling cudaGetDeviceCount\n");
        return -1;
    }
    printf("found %d devices\n", numDevices);
    cudaSetDevice(0);
    return 0;
}
