#include <stdio.h>

#include <__cudaFatFormat.h>
#include <driver_types.h>
#include <vector_types.h>
#include "../cudart.h"

cudaError_t myCudaGetDeviceCount(int * count)
{
    cudaError_t ret = cudaSuccess;
    printf("%s called\n", __func__);
    if (count) {
        *count = 1337;
    } else {
        ret = cudaErrorInvalidValue;
    }
    return ret;
}

/* called upon load of the library, before app's main */
void cudaJmpTblConstructor(void)
{
    printf("%s setting cudaGetDeviceCount\n", __func__);
    preCudaJmp.cudaGetDeviceCount = myCudaGetDeviceCount;
}
