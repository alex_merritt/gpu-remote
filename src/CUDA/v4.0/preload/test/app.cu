#include <cuda_runtime_api.h>

__global__ void runsOnGPU(void) { ; }

int main(void)
{
    cudaSetDevice(0);
    runsOnGPU<<<1,1,1>>>();
    return 0;
}
