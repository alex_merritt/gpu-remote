#include <stdio.h>

#include <__cudaFatFormat.h>
#include <driver_types.h>
#include <vector_types.h>
#include "../precudart.h"

cudaError_t myCudaGetDeviceCount(int * count)
{
    cudaError_t ret = cudaSuccess;
    printf("%s called\n", __func__);
    if (count) {
        *count = 1337;
    } else {
        ret = cudaErrorInvalidValue;
    }
    return ret;
}

cudaError_t myCudaSetDevice(int device)
{
    return cudaSuccess;
}

/* called upon load of the library, before app's main */
void cudaJmpTblConstructor(void)
{
    preCudaJmp.cudaGetDeviceCount = myCudaGetDeviceCount;
    preCudaJmp.cudaSetDevice = myCudaSetDevice;
}
