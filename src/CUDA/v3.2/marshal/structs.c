// Structures found in the API and other headers.

//
// THIS FILE WAS GENERATED BY THE CUDA PARSER - DO NOT EDIT
//

/* typedef ptrdiff_t - not a struct */

/* typedef size_t - not a struct */

/* typedef float_t - not a struct */

/* typedef double_t - not a struct */

/* typedef _LIB_VERSION_TYPE - not a struct */

struct cudaChannelFormatDesc
{
    int x
    int y
    int z
    int w
    enum cudaChannelFormatKind f
}

struct cudaArray
{
    /* unknown member(s) or empty structure */
}

struct cudaPitchedPtr
{
    void * ptr
    size_t pitch
    size_t xsize
    size_t ysize
}

struct cudaExtent
{
    size_t width
    size_t height
    size_t depth
}

struct cudaPos
{
    size_t x
    size_t y
    size_t z
}

struct cudaMemcpy3DParms
{
    struct cudaArray * srcArray
    struct cudaPos srcPos
    struct cudaPitchedPtr srcPtr
    struct cudaArray * dstArray
    struct cudaPos dstPos
    struct cudaPitchedPtr dstPtr
    struct cudaExtent extent
    enum cudaMemcpyKind kind
}

struct cudaGraphicsResource
{
    /* unknown member(s) or empty structure */
}

struct cudaFuncAttributes
{
    size_t sharedSizeBytes
    size_t constSizeBytes
    size_t localSizeBytes
    int maxThreadsPerBlock
    int numRegs
    int ptxVersion
    int binaryVersion
    int [6] __cudaReserved
}

struct cudaDeviceProp
{
    char [256] name
    size_t totalGlobalMem
    size_t sharedMemPerBlock
    int regsPerBlock
    int warpSize
    size_t memPitch
    int maxThreadsPerBlock
    int [3] maxThreadsDim
    int [3] maxGridSize
    int clockRate
    size_t totalConstMem
    int major
    int minor
    size_t textureAlignment
    int deviceOverlap
    int multiProcessorCount
    int kernelExecTimeoutEnabled
    int integrated
    int canMapHostMemory
    int computeMode
    int maxTexture1D
    int [2] maxTexture2D
    int [3] maxTexture3D
    int [3] maxTexture2DArray
    size_t surfaceAlignment
    int concurrentKernels
    int ECCEnabled
    int pciBusID
    int pciDeviceID
    int tccDriver
    int [21] __cudaReserved
}

/* typedef cudaError_t - not a struct */

struct CUstream_st
{
    /* unknown member(s) or empty structure */
}

/* typedef cudaStream_t - not a struct */

struct CUevent_st
{
    /* unknown member(s) or empty structure */
}

/* typedef cudaEvent_t - not a struct */

/* typedef cudaGraphicsResource_t - not a struct */

struct CUuuid_st
{
    /* unknown member(s) or empty structure */
}

typedef struct cudaUUID_t
{
    /* unknown member(s) or empty structure */
}

struct surfaceReference
{
    struct cudaChannelFormatDesc channelDesc
}

struct textureReference
{
    int normalized
    enum cudaTextureFilterMode filterMode
    enum cudaTextureAddressMode [3] addressMode
    struct cudaChannelFormatDesc channelDesc
    int [16] __cudaReserved
}

struct char1
{
    signed char x
}

struct uchar1
{
    unsigned char x
}

struct char2
{
    signed char x
    signed char y
}

struct uchar2
{
    unsigned char x
    unsigned char y
}

struct char3
{
    signed char x
    signed char y
    signed char z
}

struct uchar3
{
    unsigned char x
    unsigned char y
    unsigned char z
}

struct char4
{
    signed char x
    signed char y
    signed char z
    signed char w
}

struct uchar4
{
    unsigned char x
    unsigned char y
    unsigned char z
    unsigned char w
}

struct short1
{
    short x
}

struct ushort1
{
    unsigned short x
}

struct short2
{
    short x
    short y
}

struct ushort2
{
    unsigned short x
    unsigned short y
}

struct short3
{
    short x
    short y
    short z
}

struct ushort3
{
    unsigned short x
    unsigned short y
    unsigned short z
}

struct short4
{
    short x
    short y
    short z
    short w
}

struct ushort4
{
    unsigned short x
    unsigned short y
    unsigned short z
    unsigned short w
}

struct int1
{
    int x
}

struct uint1
{
    unsigned int x
}

struct int2
{
    int x
    int y
}

struct uint2
{
    unsigned int x
    unsigned int y
}

struct int3
{
    int x
    int y
    int z
}

struct uint3
{
    unsigned int x
    unsigned int y
    unsigned int z
}

struct int4
{
    int x
    int y
    int z
    int w
}

struct uint4
{
    unsigned int x
    unsigned int y
    unsigned int z
    unsigned int w
}

struct long1
{
    long x
}

struct ulong1
{
    unsigned long x
}

struct long2
{
    long x
    long y
}

struct ulong2
{
    unsigned long x
    unsigned long y
}

struct long3
{
    long x
    long y
    long z
}

struct ulong3
{
    unsigned long x
    unsigned long y
    unsigned long z
}

struct long4
{
    long x
    long y
    long z
    long w
}

struct ulong4
{
    unsigned long x
    unsigned long y
    unsigned long z
    unsigned long w
}

struct float1
{
    float x
}

struct float2
{
    float x
    float y
}

struct float3
{
    float x
    float y
    float z
}

struct float4
{
    float x
    float y
    float z
    float w
}

struct longlong1
{
    long long x
}

struct ulonglong1
{
    unsigned long long x
}

struct longlong2
{
    long long x
    long long y
}

struct ulonglong2
{
    unsigned long long x
    unsigned long long y
}

struct longlong3
{
    long long x
    long long y
    long long z
}

struct ulonglong3
{
    unsigned long long x
    unsigned long long y
    unsigned long long z
}

struct longlong4
{
    long long x
    long long y
    long long z
    long long w
}

struct ulonglong4
{
    unsigned long long x
    unsigned long long y
    unsigned long long z
    unsigned long long w
}

struct double1
{
    double x
}

struct double2
{
    double x
    double y
}

struct double3
{
    double x
    double y
    double z
}

struct double4
{
    double x
    double y
    double z
    double w
}

struct dim3
{
    unsigned int x
    unsigned int y
    unsigned int z
}

struct 
{
    char * gpuProfileName
    char * cubin
}

typedef struct __cudaFatCubinEntry
{
    char * gpuProfileName
    char * cubin
}

typedef struct __cudaFatPtxEntry
{
    char * gpuProfileName
    char * ptx
}

struct __cudaFatDebugEntryRec
{
    char * gpuProfileName
    char * debug
    struct __cudaFatDebugEntryRec * next
    unsigned int size
}

typedef struct __cudaFatDebugEntry
{
    char * gpuProfileName
    char * debug
    struct __cudaFatDebugEntryRec * next
    unsigned int size
}

struct __cudaFatElfEntryRec
{
    char * gpuProfileName
    char * elf
    struct __cudaFatElfEntryRec * next
    unsigned int size
}

typedef struct __cudaFatElfEntry
{
    char * gpuProfileName
    char * elf
    struct __cudaFatElfEntryRec * next
    unsigned int size
}

/* typedef __cudaFatCudaBinaryFlag - not a struct */

typedef struct __cudaFatSymbol
{
    char * name
}

struct __cudaFatCudaBinaryRec
{
    unsigned long magic
    unsigned long version
    unsigned long gpuInfoVersion
    char * key
    char * ident
    char * usageMode
    __cudaFatPtxEntry * ptx
    __cudaFatCubinEntry * cubin
    __cudaFatDebugEntry * debug
    void * debugInfo
    unsigned int flags
    __cudaFatSymbol * exported
    __cudaFatSymbol * imported
    struct __cudaFatCudaBinaryRec * dependends
    unsigned int characteristic
    __cudaFatElfEntry * elf
}

typedef struct __cudaFatCudaBinary
{
    unsigned long magic
    unsigned long version
    unsigned long gpuInfoVersion
    char * key
    char * ident
    char * usageMode
    __cudaFatPtxEntry * ptx
    __cudaFatCubinEntry * cubin
    __cudaFatDebugEntry * debug
    void * debugInfo
    unsigned int flags
    __cudaFatSymbol * exported
    __cudaFatSymbol * imported
    struct __cudaFatCudaBinaryRec * dependends
    unsigned int characteristic
    __cudaFatElfEntry * elf
}

/* typedef __cudaFatCompilationPolicy - not a struct */

